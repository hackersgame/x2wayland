#!/usr/bin/python3
import os

if os.geteuid() != 0:
    print("Run as root")
    exit(1)
os.environ['DISPLAY'] = ":0"
os.system("su - purism -c 'DISPLAY=:0 xhost +'")
from pynput.keyboard import Key, Listener
from evdev import UInput, ecodes as e
#############################################

repeat_key = True
#setup display var:
#key output
ui = UInput()

#########################################################setup scancodes##############################################
scancodes = {
    # Scancode: ASCIICode
    0: None, 1: u'ESC', 2: u'1', 3: u'2', 4: u'3', 5: u'4', 6: u'5', 7: u'6', 8: u'7', 9: u'8',
    10: u'9', 11: u'0', 12: u'-', 13: u'=', 14: u'BKSP', 15: u'TAB', 16: u'q', 17: u'w', 18: u'e', 19: u'r',
    20: u't', 21: u'y', 22: u'u', 23: u'i', 24: u'o', 25: u'p', 26: u'[', 27: u']', 28: u'CRLF', 29: u'LEFTCTRL',
    30: u'a', 31: u's', 32: u'd', 33: u'f', 34: u'g', 35: u'h', 36: u'j', 37: u'k', 38: u'l', 39: u';',
    40: u'"', 41: u'`', 42: u'LEFTSHFT', 43: u'\\', 44: u'z', 45: u'x', 46: u'c', 47: u'v', 48: u'b', 49: u'n',
    50: u'm', 51: u',', 52: u'.', 53: u'/', 54: u'RIGHTSHFT', 56: u'LEFTALT', 57: u' ', 100: u'RIGHTALT'
}
#reversed version
scancodes = dict(map(reversed, scancodes.items()))
capscodes = {
    0: None, 1: u'ESC', 2: u'!', 3: u'@', 4: u'#', 5: u'$', 6: u'%', 7: u'^', 8: u'&', 9: u'*',
    10: u'(', 11: u')', 12: u'_', 13: u'+', 14: u'BKSP', 15: u'TAB', 16: u'Q', 17: u'W', 18: u'E', 19: u'R',
    20: u'T', 21: u'Y', 22: u'U', 23: u'I', 24: u'O', 25: u'P', 26: u'{', 27: u'}', 28: u'CRLF', 29: u'LEFTCTRL',
    30: u'A', 31: u'S', 32: u'D', 33: u'F', 34: u'G', 35: u'H', 36: u'J', 37: u'K', 38: u'L', 39: u':',
    40: u'\'', 41: u'~', 42: u'LEFTSHFT', 43: u'|', 44: u'Z', 45: u'X', 46: u'C', 47: u'V', 48: u'B', 49: u'N',
    50: u'M', 51: u'<', 52: u'>', 53: u'?', 54: u'RIGHTSHFT', 56: u'LEFTALT',  57: u' ', 100: u'RIGHTALT'
}
#reversed version
capscodes = dict(map(reversed, capscodes.items()))

#add capscodes to scancodes
scancodes.update(capscodes)
####################################################################################################################

def guess_key(key_name, last_try=False):
    if key_name in scancodes:
        key_code = scancodes[key_name]
        return(key_code)
    dict_name = "KEY_" + key_name.upper()
    if dict_name in e.ecodes:
        return(e.ecodes[dict_name])
    else:
        #try KEY_LEFT<whatever>
        if not last_try:
            return(guess_key("LEFT" + key_name, last_try=True))
        print("Unhandled key: " + key_name)
        return(None)

#reading keys
##############################################################################
keys_down = []
def on_press(key):
    global keys_down
    #clean up key name:
    try:
        key = key.char
    except AttributeError:
        key = key.name
    

    key_code = guess_key(key)
    if key_code is None:
        return(None)
    
    #BUG, X11 apps will endlessly repeat the same keys
    if repeat_key:
        ui.write(e.EV_KEY, key_code, 1)
        ui.syn()
    print(f'{guess_key(key)} down')
    if key not in keys_down:
        keys_down.append(key)
                                                                             #
                                                                             #
def on_release(key):
    global keys_down
                                                                             #
    #clean up key name:
    try:
        key = key.char
    except AttributeError:
        key = key.name
    
    key_code = guess_key(key)
    if key_code is not None and repeat_key:
        ui.write(e.EV_KEY, key_code, 0)
        ui.syn()#
    print(f'{key} up')
    if key in keys_down:
        keys_down.remove(key)

with Listener(on_press=on_press,on_release=on_release) as listener:
    listener.join()
##############################################################################

ui.close()
