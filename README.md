#Share your desktop keyboard with the Librem 5

On Librem 5:
sudo apt install x2x
sudo pip3 install pynput
Copy x2wayland.py to ~/
run: chmod 755 ~/x2wayland.py

From Desktop:
run: ssh -X purism@<Librem 5 IP> 'x2x -east -to :0 & sudo ~/x2wayland.py'
